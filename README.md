INTRODUCTION
------------
This module extends the functionality of role reference fields by providing custom widgets tailored for role selection. With this module, users can select allowed roles, ensuring that only those roles are visible on the form. Two custom widgets, namely 'user_role_checkbox_widget' and 'user_role_select_list_widget', are available for use with role reference fields.

*Upon selecting either the 'user_role_checkbox_widget' or 'user_role_select_list_widget', users can configure the allowed roles in the settings form. 



INSTALLATION
------------

* Install the user role widget module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.

  * Navigate to manage fields of any content type and add user reference field and choose role reference type.
  * Navigate to manage display and choose user role widget.
