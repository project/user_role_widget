<?php

namespace Drupal\user_role_widget\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Provides specific access control for the user entity type.
 *
 * @EntityReferenceSelection(
 *   id = "default:custom_user_role",
 *   label = @Translation("Role selection"),
 *   entity_types = {"user_role"},
 *   group = "default",
 *   weight = 1
 * )
 */
class RoleSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);

    $configuration = $this->getConfiguration();

    if (!empty($configuration['roles'])) {
      $role_array = $configuration['roles'];

      // Remove keys with a value of 0.
      $role_array = array_filter($role_array, function ($value) {
        return $value !== 0;
      });

      // Iterate through the array and capitalize the labels.
      foreach ($role_array as $key => $value) {

        // Capitalize the first letter of each word.
        $label = ucwords(str_replace('_', ' ', $value));

        if ($label === 'Authenticated' || $label === 'Anonymous') {
          $label = $label . ' user';
        }
        // Replace the label in the array.
        $role_array[$key] = $label;
      }

      $query->condition('label', $role_array, 'IN');
    }

    return $query;
  }

}
