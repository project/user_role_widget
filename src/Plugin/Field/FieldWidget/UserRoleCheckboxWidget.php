<?php

namespace Drupal\user_role_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Plugin implementation of the 'user_role_checkbox_widget' widget.
 *
 * @FieldWidget(
 *   id = "user_role_checkbox_widget",
 *   label = @Translation("User Role Checkbox Widget"),
 *   description = @Translation("A checkbox widget with custom settings."),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class UserRoleCheckboxWidget extends OptionsButtonsWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'roles' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    // Get the available roles.
    $roles = Role::loadMultiple();
    $options = [];

    foreach ($roles as $id => $role) {
      $options[$id] = $role->label();
    }

    $selected_roles = $form_state->get('roles') ?? $this->getSetting('roles');
    $element['roles'] = [
      '#title' => $this->t('Roles'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $selected_roles,
      '#description' => $this->t('Select roles to display as options.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Get the selected roles from the widget settings.
    $selected_roles = $this->getSetting('roles');
    // Get the available roles.
    $roles = Role::loadMultiple();
    $options = [];

    foreach ($roles as $id => $role) {
      $options[$id] = $role->label();
    }

    // Create a summary of selected roles.
    $selected_role_labels = [];
    foreach ($selected_roles as $role_id) {
      if ($role_id !== 0) {
        $selected_role_labels[] = $options[$role_id];
      }
    }
    // Check if there are any selected roles before adding the comma.
    if (!empty($selected_role_labels)) {
      $summary[] = $this->t('Selected Roles: @roles', ['@roles' => implode(', ', $selected_role_labels)]);
    }
    else {
      $summary[] = $this->t('No roles selected');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $all_options = $this->getOptions($items->getEntity());

    // Get the field definition.
    $field_definition = $items->getFieldDefinition();

    // Get the field storage type.
    $field_storage = $field_definition->getFieldStorageDefinition();

    // Get the referenced entity type.
    $referenced_entity_type = $field_storage->getSetting('target_type');

    if ($referenced_entity_type === 'user_role') {
      // Get the selected roles from the widget settings.
      $allowed_roles = $this->getSetting('roles');

      $selectedRoles = [];
      foreach ($allowed_roles as $role => $value) {
        if ($value !== 0) {
          $selectedRoles[$role] = $role;
        }
      }

      $options = array_intersect_key($all_options, $selectedRoles);

      $element['#options'] = $options;
    }

    return $element;

  }

}
